/**
 * Created by Pawel Polit
 */

/**
 * @memberOf   express
 * or
 * @name       express.use
 */
/**
 * @memberOf   express
 * or
 * @name       express.redirect
 */

var express = require('express');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var fs = require('fs');

var server = express();
server.use(cookieParser());
server.use(session({secret: 'someSecret', saveUninitialized: true, resave: true}));


var userIdCounter = 1;

var idToUser = {
    0: 'admin'
};

var loggedUsersIds = [];

var adminRedirection = 'admin';


var tableContains = function(p_table, p_element) {
    for(var i = 0; i < p_table.length; ++i) {
        if(p_table[i] === p_element) {
            return true;
        }
    }
    return false;
};

var urlEndsWith = function(p_suffix, p_url) {
    return p_url.indexOf(p_suffix, p_url.length - p_suffix.length) !== -1;
};


var parseUserToPassword = function() {
    return JSON.parse(fs.readFileSync('server/userToPassword.json'));
};

var parseUserToCategories = function() {
    return JSON.parse(fs.readFileSync('server/userToCategories.json'));
};

var parseUserCategoryToNameAndURL = function() {
    return JSON.parse(fs.readFileSync('server/userCategoryToNameAndURL.json'));
};


server.get('/', function(request, response) {

    if(!tableContains(loggedUsersIds, request.session.userId)) {
        response.redirect('/login');
    } else {
        fs.readFile('website/index.html', function(err, html) {
            if(err) {
                response.writeHead(500, {'Content-Type': 'text/plain'});
                response.end('Internal Server Error');
            } else {
                response.writeHead(200, {'Content-Type': 'text/html'});
                response.end(html);
            }
        });
    }
});

server.get('/background.jpg', function(request, response) {
    fs.readFile('website/background.jpg', function(err, background) {
        if(err) {
            response.writeHead(500, {'Content-Type': 'text/plain'});
            response.end('Internal Server Error');
        } else {
            response.writeHead(200, {'Content-Type': 'image/jpeg'});
            response.end(background);
        }
    });
});

server.get('/css/stylesheet.css', function(request, response) {

    if(!tableContains(loggedUsersIds, request.session.userId)) {
        response.redirect('/login');
    } else {
        fs.readFile('website/stylesheet.css', function(err, css) {
            if(err) {
                response.writeHead(500, {'Content-Type': 'text/plain'});
                response.end('Internal Server Error');
            } else {
                response.writeHead(200, {'Content-Type': 'text/css'});
                response.end(css);
            }
        });
    }
});

server.get('/javascript/script.js', function(request, response) {

    if(!tableContains(loggedUsersIds, request.session.userId)) {
        response.redirect('/login');
    } else {
        fs.readFile('website/script.js', function(err, script) {
            if(err) {
                response.writeHead(500, {'Content-Type': 'text/plain'});
                response.end('Internal Server Error');
            } else {
                response.writeHead(200, {'Content-Type': 'text/javascript'});
                response.end(script);
            }
        });
    }
});

server.get('/json/categories', function(request, response) {

    if(!tableContains(loggedUsersIds, request.session.userId)) {
        response.writeHead(401, {'Content-Type': 'text/plain'});
        response.end();
    } else {
        var user;

        if(request.session.userId === 0) {
            user = adminRedirection;
        } else {
            user = idToUser[request.session.userId];
        }

        var categories = parseUserToCategories()[user];

        response.writeHead(200, {'Content-Type': 'application/json'});
        response.end(JSON.stringify(categories));
    }
});

server.get('/json/categoryToNameAndURL', function(request, response) {

    if(!tableContains(loggedUsersIds, request.session.userId)) {
        response.writeHead(401, {'Content-Type': 'text/plain'});
        response.end();
    } else {
        var user;

        if(request.session.userId === 0) {
            user = adminRedirection;
        } else {
            user = idToUser[request.session.userId];
        }

        var categoryToNameAndURL = parseUserCategoryToNameAndURL()[user];

        response.writeHead(200, {'Content-Type': 'application/json'});
        response.end(JSON.stringify(categoryToNameAndURL));
    }
});

server.get('/login', function(request, response) {

    if(tableContains(loggedUsersIds, request.session.userId)) {
        response.redirect('/');
    } else {
        fs.readFile('website/login.html', function(err, html) {
            if(err) {
                response.writeHead(500, {'Content-Type': 'text/plain'});
                response.end('Internal Server Error');
            } else {

                response.writeHead(200, {'Content-Type': 'text/html'});
                response.end(html);
            }
        });
    }
});

server.get('/css/login_stylesheet.css', function(request, response) {
    fs.readFile('website/login_stylesheet.css', function(err, css) {
        if(err) {
            response.writeHead(500, {'Content-Type': 'text/plain'});
            response.end('Internal Server Error');
        } else {
            response.writeHead(200, {'Content-Type': 'text/css'});
            response.end(css);
        }
    });
});

server.get('/javascript/login_script.js', function(request, response) {
    fs.readFile('website/login_script.js', function(err, script) {
        if(err) {
            response.writeHead(500, {'Content-Type': 'text/plain'});
            response.end('Internal Server Error');
        } else {
            response.writeHead(200, {'Content-Type': 'text/javascript'});
            response.end(script);
        }
    });
});

server.post('/login', function(request, response) {
    if(tableContains(loggedUsersIds, request.session.userId)) {
        response.writeHead(406, {'Content-Type': 'text/plain'});
        response.end();
    } else {
        var userToPassword = parseUserToPassword();

        request.on('data', function(chunk) {
            var content = JSON.parse(chunk.toString());
            var user = content.user;
            var password = content.password;

            if(userToPassword[user] === password) {
                if(user === 'admin') {
                    request.session.userId = 0;
                    loggedUsersIds.push(0);
                } else {
                    request.session.userId = userIdCounter;
                    loggedUsersIds.push(userIdCounter);
                    idToUser[userIdCounter] = user;
                    ++userIdCounter;
                }
                response.writeHead(200, {'Content-Type': 'text/plain'});
                response.end('http://localhost:4567');
            } else if(userToPassword[user] === undefined) {
                response.writeHead(418, {'Content-Type': 'text/plain'});
                response.end();
            } else {
                response.writeHead(400, {'Content-Type': 'text/plain'});
                response.end();
            }
        });
    }
});

server.get('/logout', function(request, response) {
    var userId = request.session.userId;
    delete request.session.userId;

    if(userId === 0) {
        adminRedirection = 'admin';
    }

    var index = loggedUsersIds.indexOf(userId);
    if(index >= 0) {
        loggedUsersIds.splice(index, 1);
        if(userId !== 0) {
            delete idToUser[userId];
        }
    }
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('http://localhost:4567/login');
});

server.post('/register', function(request, response) {
    var userToPassword = parseUserToPassword();

    request.on('data', function(chunk) {
        var content = JSON.parse(chunk.toString());
        var user = content.user;
        var password = content.password;

        if(userToPassword[user] === undefined) {
            userToPassword[user] = password;

            var userToCategories = parseUserToCategories();
            userToCategories[user] = [];

            var userCategoryToNameAndURL = parseUserCategoryToNameAndURL();
            userCategoryToNameAndURL[user] = {};

            fs.writeFileSync('server/userToPassword.json', JSON.stringify(userToPassword));
            fs.writeFileSync('server/userToCategories.json', JSON.stringify(userToCategories));
            fs.writeFileSync('server/userCategoryToNameAndURL.json', JSON.stringify(userCategoryToNameAndURL));

            response.writeHead(201, {'Content-Type': 'text/plain'});
            response.end();

        } else {
            response.writeHead(400, {'Content-Type': 'text/plain'});
            response.end();
        }
    });
});

server.get('/whoami', function(request, response) {
    var userId = request.session.userId;

    if(!tableContains(loggedUsersIds, userId)) {
        response.writeHead(401, {'Content-Type': 'text/plain'});
        response.end();
    } else {
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end(idToUser[userId]);
    }
});

server.delete('/category/:user/:category', function(request, response) {
    var userId = request.session.userId;
    var user = request.params['user'];
    var category = request.params['category'];

    if(userId === 0 || idToUser[userId] === user) {
        if(userId === 0) {
            user = adminRedirection;
        }

        var userToCategories = parseUserToCategories();
        var userCategoryToNameAndURL = parseUserCategoryToNameAndURL();

        var index = userToCategories[user].indexOf(category);

        if(index >= 0) {
            userToCategories[user].splice(index, 1);
            delete userCategoryToNameAndURL[user][category];
            fs.writeFileSync('server/userToCategories.json', JSON.stringify(userToCategories));
            fs.writeFileSync('server/userCategoryToNameAndURL.json', JSON.stringify(userCategoryToNameAndURL));
        }

        response.writeHead(204, {'Content-Type': 'text/plain'});
        response.end();

    } else {
        response.writeHead(401, {'Content-Type': 'text/plain'});
        response.end();
    }
});

server.delete('/movie/:user/:category/:movieId', function(request, response) {
    var userId = request.session.userId;
    var user = request.params['user'];
    var category = request.params['category'];
    var movieId = request.params['movieId'];

    if(userId === 0 || idToUser[userId] === user) {
        if(userId === 0) {
            user = adminRedirection;
        }

        var userCategoryToNameAndURL = parseUserCategoryToNameAndURL();

        for(var i = 0; i < userCategoryToNameAndURL[user][category].length; ++i) {
            if(urlEndsWith(movieId, userCategoryToNameAndURL[user][category][i].url)) {
                userCategoryToNameAndURL[user][category].splice(i, 1);
                fs.writeFileSync('server/userCategoryToNameAndURL.json', JSON.stringify(userCategoryToNameAndURL));
                break;
            }
        }

        response.writeHead(204, {'Content-Type': 'text/plain'});
        response.end();

    } else {
        response.writeHead(401, {'Content-Type': 'text/plain'});
        response.end();
    }
});

server.post('/create/category', function(request, response) {
    request.on('data', function(chunk) {
        var content = JSON.parse(chunk.toString());
        var userId = request.session.userId;
        var user = content['user'];
        var category = content['category'];

        if(userId === 0 || idToUser[userId] === user) {
            if(userId === 0) {
                user = adminRedirection;
            }

            var userToCategories = parseUserToCategories();
            var userCategoryToNameAndURL = parseUserCategoryToNameAndURL();

            userToCategories[user].push(category);
            userCategoryToNameAndURL[user][category] = [];

            fs.writeFileSync('server/userToCategories.json', JSON.stringify(userToCategories));
            fs.writeFileSync('server/userCategoryToNameAndURL.json', JSON.stringify(userCategoryToNameAndURL));

            response.writeHead(201, {'Content-Type': 'text/plain'});
            response.end();
        } else {
            response.writeHead(401, {'Content-Type': 'text/plain'});
            response.end();
        }
    });
});

server.post('/create/movie', function(request, response) {
    request.on('data', function(chunk) {
        var content = JSON.parse(chunk.toString());
        var userId = request.session.userId;
        var user = content['user'];
        var category = content['category'];
        var movieName = content['movieName'];
        var movieUrl = content['movieUrl'];

        if(userId === 0 || idToUser[userId] === user) {
            if(userId === 0) {
                user = adminRedirection;
            }

            var userCategoryToNameAndURL = parseUserCategoryToNameAndURL();

            userCategoryToNameAndURL[user][category].push({
                'name': movieName,
                'url': movieUrl
            });

            fs.writeFileSync('server/userCategoryToNameAndURL.json', JSON.stringify(userCategoryToNameAndURL));

            response.writeHead(201, {'Content-Type': 'text/plain'});
            response.end();
        } else {
            response.writeHead(401, {'Content-Type': 'text/plain'});
            response.end();
        }
    });
});

server.delete('/user/:user', function(request, response) {
    var userId = request.session.userId;
    var user = request.params['user'];

    if(user === 'admin') {
        response.writeHead(403, {'Content-Type': 'text/plain'});
        response.end();
    } else if(userId === 0 || idToUser[userId] === user) {
        var userToPassword = parseUserToPassword();
        var userToCategories = parseUserToCategories();
        var userCategoryToNameAndURL = parseUserCategoryToNameAndURL();

        delete userToPassword[user];
        delete userToCategories[user];
        delete userCategoryToNameAndURL[user];

        fs.writeFileSync('server/userToPassword.json', JSON.stringify(userToPassword));
        fs.writeFileSync('server/userToCategories.json', JSON.stringify(userToCategories));
        fs.writeFileSync('server/userCategoryToNameAndURL.json', JSON.stringify(userCategoryToNameAndURL));

        for(var i = 0; i < loggedUsersIds.length; ++i) {
            if(idToUser[loggedUsersIds[i]] === user) {
                delete idToUser[loggedUsersIds[i]];
                loggedUsersIds.splice(i, 1);
                break;
            }
        }

        if(userId === 0) {
            response.writeHead(204, {'Content-Type': 'text/plain'});
            response.end();
        } else {
            response.writeHead(200, {'Content-Type': 'text/plain'});
            response.end('http://localhost:4567/login');
        }
    } else {
        response.writeHead(401, {'Content-Type': 'text/plain'});
        response.end();
    }
});

server.get('/admin', function(request, response) {
    var userId = request.session.userId;

    if(userId === 0) {
        var userToPassword = parseUserToPassword();

        response.writeHead(200, {'Content-Type': 'application/json'});
        response.end(JSON.stringify(Object.keys(userToPassword)));
    } else {
        response.writeHead(401, {'Content-Type': 'text/plain'});
        response.end();
    }
});

server.get('/admin/redirect/:user', function(request, response) {
    if(request.session.userId !== 0) {
        response.writeHead(401, {'Content-Type': 'text/plain'});
        response.end();
    } else {
        var user = request.params['user'];

        var userToPassword = parseUserToPassword();
        if(userToPassword[user] === undefined) {
            response.writeHead(404, {'Content-Type': 'text/plain'});
            response.end();
        } else {
            adminRedirection = user;
            response.writeHead(200, {'Content-Type': 'text/plain'});
            response.end('http://localhost:4567');
        }
    }
});

server.get('/admin/redirection', function(request, response) {
    if(request.session.userId !== 0) {
        response.writeHead(401, {'Content-Type': 'text/plain'});
        response.end();
    } else {
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end(adminRedirection);
    }
});

server.listen(4567, function() {
    console.log('Server running at http://localhost:4567/');
});

