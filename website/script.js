/**
 * Created by Pawel Polit
 */

var currentPlaylistId = 1;
var idForNewPlaylist = 2;
var numberOfExistingPlaylists = 0;
var currentUser = '';
var adminPanelHidden = false;


var httpGET = function(p_url) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('GET', p_url, false);
    xmlHttp.send(null);
    return xmlHttp;
};

var httpDELETE = function(p_url) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('DELETE', p_url, false);
    xmlHttp.send(null);
    return xmlHttp;
};

var httpPOST = function(p_url, p_json) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('POST', p_url, false);
    xmlHttp.setRequestHeader('Content-type', 'application/json');
    xmlHttp.send(p_json);
    return xmlHttp;
};

var getIdFromUrl = function(p_url) {
    var index = p_url.lastIndexOf('/');
    return p_url.substring(index + 1);
};

var deleteMovieAction = function() {
    if(confirm('Do you really want to remove this movie ?')) {
        var movieUrl = $(this).parent().parent().find('iframe').attr("src");
        var movieId = getIdFromUrl(movieUrl);

        var playlistId = $(this).parent().parent().parent().attr("id");
        var category = $('#-' + playlistId).clone().children().remove().end().text();

        if(httpDELETE('http://localhost:4567/movie/' + currentUser + '/' + category + '/' + movieId).status == 204) {
            $(this).parent().parent().remove();
        } else {
            alert("You cannot remove this movie");
        }
    }
};

var prepareCorrectLink = function(p_movieLink) {
    return p_movieLink.replace('watch?v=', 'embed/');
};

var prepareNewMovieElement = function(p_movieName, p_movieLink, p_preparing) {
    var $name = $('<h3></h3>');
    $name.addClass('button');
    $name.html(p_movieName);

    var $deleteButton = $('<div></div>');
    $deleteButton.addClass('delete-button');
    $deleteButton.html('X');

    if(!p_preparing) {
        $deleteButton.click(deleteMovieAction);
    }

    var $firstDiv = $('<div></div>');
    $firstDiv.append($name);
    $firstDiv.append($deleteButton);

    var $iframe = $('<iframe allowfullscreen></iframe>');
    $iframe.attr({
        width: '800',
        height: '480',
        src: p_movieLink,
        frameBorder: '0'
    });

    var $secondDiv = $('<div></div>');
    $secondDiv.append($iframe);

    var $result = $('<div></div>');
    $result.addClass('collapsible');
    $result.append($firstDiv);
    $result.append($secondDiv);

    return $result;
};

var linkPrefix = 'https://www.youtube.com/watch?v=';

var linkIsCorrect = function(p_movieLink) {
    return p_movieLink.slice(0, linkPrefix.length) === linkPrefix;
};

var hideLastHr = function() {
    $('.playlist hr').last().hide();
};

var clickCategoryAction = function() {
    var nextPlaylistId = -parseInt($(this).attr('id'));

    if(nextPlaylistId != currentPlaylistId) {

        if(nextPlaylistId === 1) {
            $('#' + currentPlaylistId).fadeOut('slow', function() {
                $('.playlist').fadeIn('slow');
                $('hr').show();
                hideLastHr()
            });
        } else if(currentPlaylistId === 1) {
            $('.playlist').fadeOut('slow', function() {
                $('#' + nextPlaylistId).fadeIn('slow');
                $('hr').hide();
            });
        } else {
            $('#' + currentPlaylistId).fadeOut('slow', function() {
                $('#' + nextPlaylistId).fadeIn('slow');
            });
        }
        currentPlaylistId = nextPlaylistId;
    }
    $(this).parent().parent().accordion({active: false});
};

var deleteCategoryAction = function() {
    var playlistId = -parseInt($(this).parent().attr('id'));

    if(currentPlaylistId === playlistId) {
        alert("You can't remove a category that is open !!!");
        return;
    }

    if(confirm('Do you really want to remove this category ? (This action will remove all movies related to this category)')) {

        var category = $(this).parent().clone().children().remove().end().text();

        if(httpDELETE('http://localhost:4567/category/' + currentUser + '/' + category).status == 204) {
            $(this).parent().remove();
            $('#' + playlistId).fadeOut('slow', function() {
                $('#' + playlistId).remove();
                hideLastHr();
            });
            numberOfExistingPlaylists--;
            $('#categories').css({height: '-=50px'});
        } else {
            alert('You cannot remove category: ' + category);
        }
    }
};

var prepareNewCategoryElement = function(p_categoryName, p_preparing) {
    var $deleteButton = $('<div></div>');
    $deleteButton.addClass('delete-button');
    $deleteButton.html('X');

    if(!p_preparing) {
        $deleteButton.click(deleteCategoryAction);
    }


    var $result = $('<div></div>');
    $result.addClass('category');
    $result.attr({id: '-' + idForNewPlaylist});
    $result.html(p_categoryName);
    $result.append($deleteButton);

    return $result;
};

var prepareNewPlaylistElement = function() {
    var $result = $('<div></div>');
    $result.addClass('playlist');
    $result.attr({id: idForNewPlaylist});
    $result.append('<hr>');

    return $result;
};

var prepareDocument = function() {

    var categories = JSON.parse(httpGET('http://localhost:4567/json/categories').responseText);
    var categoryToNameAndURL = JSON.parse(httpGET('http://localhost:4567/json/categoryToNameAndURL').responseText);
    currentUser = httpGET('http://localhost:4567/whoami').responseText;

    var $categories = $('#categories');
    var $body = $('body');
    var $footer = $('#footer');
    $footer.prepend("<span id='info'>You are logged in as: " + currentUser + "</span>");


    for(var i = 0; i < categories.length; ++i) {
        var $newCategory = prepareNewCategoryElement(categories[i], true);
        $newCategory.click(clickCategoryAction);
        $categories.css({height: '+=50px'});
        $categories.append($newCategory);

        var $newPlaylist = prepareNewPlaylistElement();
        $newPlaylist.sortable({
            items: '> div'
        });
        idForNewPlaylist++;

        var movies = categoryToNameAndURL[categories[i]];

        for(var j = 0; j < movies.length; ++j) {
            $newPlaylist.find('hr').first().before(prepareNewMovieElement(movies[j]['name'],
                prepareCorrectLink(movies[j]['url']), true));
        }

        $body.append($newPlaylist);
        numberOfExistingPlaylists++;
    }

    $('.collapsible').accordion({collapsible: true, active: false});

    if(currentPlaylistId === 1) {
        $('hr').show();
        hideLastHr();
    }

    var xmlHttp = httpGET('http://localhost:4567/admin');

    if(xmlHttp.status == 200) {
        var redirection = httpGET('http://localhost:4567/admin/redirection').responseText;
        var users = JSON.parse(xmlHttp.responseText);

        var $adminPanelHeader = $('<div></div>');
        $adminPanelHeader.attr({id: 'admin-panel-header'});
        $adminPanelHeader.html('Admin panel');

        var $adminPanelContent = $('<div></div>');
        $adminPanelContent.attr({id: 'admin-panel-content'});
        $adminPanelContent.html('Users:');

        for(var k = 0; k < users.length; ++k) {
            var $user = $('<div></div>');
            $user.addClass('user');

            var $userPlaylist = $('<div></div>');
            $userPlaylist.addClass('user-playlist');
            $userPlaylist.html(users[k]);

            $user.append($userPlaylist);

            if(users[k] !== 'admin') {
                var $deleteButton = $('<div></div>');
                $deleteButton.addClass('delete-button');
                $deleteButton.html('X');

                $user.append($deleteButton);
            }

            if(users[k] === redirection) {
                $user.css('background-color', 'rgba(244, 245, 255, 0.49)');
            }

            $adminPanelContent.append($user);
        }

        $adminPanelContent.sortable({
            items: '> div'
        });

        var $adminPanel = $('<div></div>');
        $adminPanel.attr({id: 'admin-panel'});
        $adminPanel.append($adminPanelHeader);
        $adminPanel.append($adminPanelContent);

        $body.append($adminPanel);
    } else {
        $footer.width('100%');
    }
};


$(document).ready(function() {
    prepareDocument();
    hideLastHr();

    $('.collapsible').accordion({collapsible: true, active: false});

    $('#categories').selectable({
        selected: function(event, ui) {
            $(ui.selected).addClass('ui-selected').siblings().removeClass('ui-selected');
        }
    });
    $('#-1').addClass('ui-selected');

    $('.playlist').sortable({
        items: '> div'
    });

    $('.playlist .delete-button').click(deleteMovieAction);

    $('#add-movie-button').click(function() {

        if(currentPlaylistId === 1) {
            alert('If you want to add movie, you have to choose category !!!');
            return;
        }

        var $currentPlaylist = $('#' + currentPlaylistId);

        var $movieNameInput = $('input[name=movieName]');
        var $movieLinkInput = $('input[name=movieLink]');

        var movieLink = $movieLinkInput.val();

        if(linkIsCorrect(movieLink)) {

            movieLink = prepareCorrectLink(movieLink);
            var movieName = $movieNameInput.val();

            var xmlHttp = httpPOST('http://localhost:4567/create/movie', JSON.stringify({
                'user': currentUser,
                'category': $('#-' + currentPlaylistId).clone().children().remove().end().text(),
                'movieName': movieName,
                'movieUrl': movieLink
            }));

            if(xmlHttp.status === 201) {
                $movieLinkInput.val('');
                $movieNameInput.val('');


                var $newElement = prepareNewMovieElement(movieName, movieLink, false);

                $currentPlaylist.find('hr').first().before($newElement);

                $('.collapsible').accordion({collapsible: true});

                $(this).parent().parent().accordion({active: false});
            } else {
                alert("You cannot create this movie");
            }
        } else {
            alert('Typed link is incorrect, try again');
        }
    });

    $('.category').click(clickCategoryAction);

    $('.header-part.collapsible').mouseleave(function() {
        $(this).accordion({active: false});
    });

    $('.category .delete-button').click(deleteCategoryAction);

    $('#add-category-button').click(function() {
        var $categoryNameInput = $('input[name=categoryName]');
        var categoryName = $categoryNameInput.val();

        if(categoryName === '') {
            if(!confirm('You are trying to add category without name, are you sure you want to do that ?')) {
                return;
            }
        }

        var xmlHttp = httpPOST('http://localhost:4567/create/category', JSON.stringify({
            'user': currentUser,
            'category': categoryName
        }));

        if(xmlHttp.status === 201) {
            $categoryNameInput.val('');

            var $newCategory = prepareNewCategoryElement(categoryName, false);
            $newCategory.click(clickCategoryAction);
            var $newPlaylist = prepareNewPlaylistElement();
            $newPlaylist.sortable({
                items: '> div'
            });
            idForNewPlaylist++;

            var $categories = $('#categories');
            $categories.css({height: '+=50px'});
            $categories.append($newCategory);
            $('body').append($newPlaylist);

            $(this).parent().parent().accordion({active: false});

            if(currentPlaylistId === 1) {
                $('hr').show();
                hideLastHr();
            }

            numberOfExistingPlaylists++;
        } else {
            alert('You cannot create category: ' + categoryName);
        }
    });

    $('#random-movie').click(function() {
        var playlistId = currentPlaylistId;
        var all = false;

        if(playlistId === 1) {
            $('div.playlist > div.collapsible').accordion({active: false});
            var playlistNumber = Math.floor(Math.random() * numberOfExistingPlaylists);
            playlistId = parseInt($('body').find('div.playlist:eq(' + playlistNumber + ')').attr('id'));
            all = true;
        }

        var $playlist = $('#' + playlistId);

        if(!all) {
            $playlist.find('div.collapsible').accordion({active: false});
        }

        var numberOfMovies = $playlist.find('div.collapsible').length;
        var movieNumber = Math.floor(Math.random() * numberOfMovies);

        var $movie = $playlist.find('div.collapsible:eq(' + movieNumber + ')');
        $playlist.prepend($movie);
        $movie.accordion({active: 0});
    });

    $('#logout-button').click(function() {
        window.open(httpGET('http://localhost:4567/logout').responseText, '_self');
    });

    $('#remove-user-button').click(function() {

        if(!confirm('Do you really want to remove your account ?')) {
            return;
        }

        var xmlHttp = httpDELETE('http://localhost:4567/user/' + currentUser);

        if(xmlHttp.status === 200) {
            window.open(xmlHttp.responseText, '_self');
        } else if(xmlHttp.status === 401) {
            alert('You cannot remove this user !!!');
        } else if(xmlHttp.status === 403) {
            alert('admin account cannot be removed !!!');
        }
    });

    $('#admin-panel-header').click(function() {
        var $adminPanel = $('#admin-panel');

        $('#admin-panel-content').slideToggle();

        if(adminPanelHidden) {
            $adminPanel.animate({height: '+=700'});
            adminPanelHidden = false;
        } else {
            $adminPanel.animate({height: '-=700'});
            adminPanelHidden = true
        }
    });

    $('.user .delete-button').click(function() {

        if(!confirm('Do you really want to remove this account ?')) {
            return;
        }

        var user = $(this).parent().find('.user-playlist').text();
        var xmlHttp = httpDELETE('http://localhost:4567/user/' + user);

        if(xmlHttp.status === 204) {
            $(this).parent().remove();
        } else if(xmlHttp.status === 401) {
            alert('You cannot remove this user !!!');
        } else if(xmlHttp.status === 403) {
            alert('admin account cannot be removed !!!');
        }
    });

    $('.user .user-playlist').click(function() {
        var user = $(this).text();

        var xmlHttp = httpGET('http://localhost:4567/admin/redirect/' + user);

        if(xmlHttp.status === 401) {
            alert('You are not admin !!!');
        } else if(xmlHttp.status === 404) {
            alert('There is no such user');
        } else if(xmlHttp.status === 200) {
            window.open(xmlHttp.responseText, '_self');
        }
    });
});