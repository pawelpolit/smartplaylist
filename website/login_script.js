/**
 * Created by Pawel Polit
 */


var httpPOST = function(p_url, p_json) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open('POST', p_url, false);
    xmlHttp.setRequestHeader('Content-type', 'application/json');
    xmlHttp.send(p_json);
    return xmlHttp;
};

$(document).ready(function() {

    $('#login-button').click(function() {
        var $userInput = $('input[name=login]');
        var $passwordInput = $('input[name=password]');

        var user = $userInput.val();
        var password = $passwordInput.val();

        var xmlHttp = httpPOST('http://localhost:4567/login', JSON.stringify({'user': user, 'password': password}));

        if(xmlHttp.status === 200) {
            window.open(xmlHttp.responseText, '_self');
        } else if(xmlHttp.status === 406) {
            alert('You are logged in !!!');
        } else if(xmlHttp.status === 418) {
            alert('There is no such user, maybe you want to register?');
        } else {
            alert('Your password is incorrect.');
        }
    });

    $('#register-button').click(function() {
        var $userInput = $('input[name=login]');
        var $passwordInput = $('input[name=password]');

        var user = $userInput.val();
        var password = $passwordInput.val();

        if(user === '' || password === '') {
            alert('You have to fill login and password !!!');
            return;
        }

        var xmlHttp = httpPOST('http://localhost:4567/register', JSON.stringify({'user': user, 'password': password}));

        if(xmlHttp.status === 201) {
            alert('Now you can log in.')
        } else {
            alert('You are already registered.');
        }
    });
});